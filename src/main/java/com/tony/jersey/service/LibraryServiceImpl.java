/**
 * 
 */
package com.tony.jersey.service;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * @author tony
 *
 */

@Path("library")
public class LibraryServiceImpl implements LibraryService {

	
	@GET
	@Path("/books")
	//@Consumes("text/*")
	@Override
	public String getBooks() {
		return "books";
	}

	@Override
	@GET
	@Path("/book/{isbn}")
	@Produces(MediaType.APPLICATION_JSON)
	public Book getBook(@PathParam("isbn") ISBN id) {//ISBN需要带一个String参数的构造方法
		Book book = new Book();
		book.setId(id);
		book.setName("java book");
		return book;
	}
	

	@Override
	@POST
	@Path("/book")
	public void addBook(Book book) {//实体参数
		//save book
		System.out.println("name:" + book.getName());
		System.out.println("id:" + book.getId());
	}

	@DELETE
	@Path("/book")
	public void removeBook(@QueryParam("id") String id) {
		//remove book
		System.out.println("remove id:" + id);
	}

	/*采用GlassFish 
	 * public static void main(String[] args) {  
        URI uri = UriBuilder.fromUri("http://127.0.0.1").port(10000).build();  
        ResourceConfig rc = new PackagesResourceConfig("com.tony.jersey.service");  
        //使用Jersey对POJO的支持，必须设置为true  
         rc.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, true);  
        try {  
            HttpServer server = GrizzlyServerFactory.createHttpServer(uri, rc);  
            server.start();  
        } catch (IllegalArgumentException e) {  
            e.printStackTrace();  
        } catch (NullPointerException e) {  
            e.printStackTrace();  
        } catch (IOException e) {  
            e.printStackTrace();  
        }  
        try {  
            Thread.sleep(1000*1000);  
        } catch (InterruptedException e) {  
            e.printStackTrace();  
        }  
    }  */
}
