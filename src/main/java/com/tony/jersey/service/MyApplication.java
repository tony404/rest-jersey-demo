/**
 * 
 */
package com.tony.jersey.service;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.codehaus.jackson.jaxrs.JacksonJsonProvider;


/**
 * @author tony
 *
 */
/*@ApplicationPath("/")
public class MyApplication extends ResourceConfig {
	public MyApplication() {
		// ����Resource
		register(LibraryServiceImpl.class);
		register(SchoolService.class);
		//ע������ת����
		register(JacksonJsonProvider.class);
	}
}*/

//@ApplicationPath("/service")
public class MyApplication extends Application {
	@Override
    public Set<Class<?>> getClasses() {
        final Set<Class<?>> classes = new HashSet<Class<?>>();
        // register root resource
        classes.add(LibraryServiceImpl.class);
      //ע������ת����
        classes.add(JacksonJsonProvider.class);
        return classes;
    }
}
