/**
 * 
 */
package com.tony.jersey.service2;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
 * @author tony
 *
 */
@Path("school2")
public class SchoolService {
	@GET
	@Path("/school")
	public String getBooks() {
		return "school";
	}
}
