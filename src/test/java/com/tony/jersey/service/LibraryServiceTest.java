package com.tony.jersey.service;
import javax.ws.rs.core.MediaType;

import org.junit.Test;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.tony.jersey.service.Book;
import com.tony.jersey.service.ISBN;

/**
 * 
 */

/**
 * @author tony
 *
 */
public class LibraryServiceTest {

	/**
	 * Test method for
	 * {@link com.tony.jersey.service.LibraryServiceImpl#getBooks()}.
	 */
	@Test
	public void testGetBooks() {
	}

	/**
	 * Test method for
	 * {@link com.tony.jersey.service.LibraryServiceImpl#getBook(com.tony.jersey.service.ISBN)}
	 * .
	 */
	@Test
	public void testGetBook() {
		Client c = Client.create();
		WebResource r = c.resource("http://localhost:8080/rest-jersey-demo/library/book/123");
		ClientResponse response = r.get(ClientResponse.class);
		//ClientResponse response = r.accept(MediaType.APPLICATION_XML).get(ClientResponse.class);//406 Not Acceptable
		System.out.println("----------getbook--------------");
		System.out.println("status:" + response.getStatus());
		System.out.println("Content-Type:" + response.getHeaders().get("Content-Type"));
		String entity = response.getEntity(String.class);
		System.out.println(entity);
	}

	/**
	 * Test method for
	 * {@link com.tony.jersey.service.LibraryServiceImpl#addBook(java.lang.String, java.lang.String)}
	 * .
	 */
	@Test
	public void testAddBook() {
		Book book = new Book();
		ISBN isbn = new ISBN();
		isbn.setIsbn("6789");
		book.setId(isbn);
		book.setName("thinking in java");

		Client c = Client.create();
		WebResource r = c.resource("http://localhost:8080/rest-jersey-demo/library/book");
		ClientResponse response = r
				.post(ClientResponse.class, book);
		System.out.println("-----------addbook----------------");
		System.out.println("status:" + response.getStatus());
		System.out.println("Content-Type:" + response.getHeaders().get("Content-Type"));

	}

	/**
	 * Test method for
	 * {@link com.tony.jersey.service.LibraryServiceImpl#removeBook(java.lang.String)}
	 * .
	 */
	@Test
	public void testRemoveBook() {
		Client c = Client.create();
		WebResource r = c.resource("http://localhost:8080/rest-jersey-demo/library/book?id=222");
		ClientResponse response = r.delete(ClientResponse.class);
		System.out.println("-------------removebook--------------");
		System.out.println("status:" + response.getStatus());
		System.out.println("Content-Type:" + response.getHeaders().get("Content-Type"));
	}

}
